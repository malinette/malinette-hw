# Malinette - MIDI
Convert your Arduino (UNO or Leonardo) to a Midi Device.
Complete documentations with images (but in french), on the [wiki] (http://reso-nance.org/wiki/logiciels/arduino-midi/accueil)

!On Pure Data, don't forger to choose the right Midi device in Media > Midi settings as input AND output

## Leonardo / Micro
* Download Arduino software (version 1.8.5)
* Follow instructions and download [TeensyDuino](https://www.pjrc.com/teensy/td_download.html)
* Check that TeensyDuino (version 1.40) support your Arduino version, listed on this first screen of installation
* Run Arduino and open File > preferences window. Add "https://adafruit.github.io/arduino-board-index/package_adafruit_index.json" in the Additionnal Boards Manager URLs field.
* Open Tools > Board > Board Manager. Search for "Adafruit TeeOnArdu" and install the package
* Select Tools > Board > TeenOnArdu (Leo on TeensyCore)
* Select Tools > USB Type > Midi
* Open the example in the "software" directory

! If you want to upload again, you will have to click on the "reset" button of the board, after the compilation. As the board is like a Midi device, the reset button let you communicate a short time in Serial to upload a new sketch.


## UNO
See the wiki documentation.