// Mecalinette
// Plans to build mecalinette > dxf for laser cutter (3mm mdf)
// http://malinette.info

// Settings
hole_d=3.1;
hole_w=13;
hole_h=3.2;
grid_w=15;
plate_margin=10;
plate_h=3;
plate_round=4;
cell_x = 9;
cell_y = 15;
//plate_w=150;
//plate_h=240;
$fn=60;

// Main geometry

projection(cut=true) mecalinette_plate_front();
translate([180,0,0]) projection(cut=true) mecalinette_plate_back();

//mecalinette_plate_back();
//mecalinette_plate_front();

// modules
module mecalinette_plate_back() {
    difference(){
         translate([plate_round,plate_round,0]) plate();
         translate([0,0,-0.1])holes_back();
    }
}

module mecalinette_plate_front() {
    difference(){
         translate([plate_round,plate_round,0]) plate();
         translate([0,0,-0.1])holes_front();
    }
}



//plate
module plate(){
    union(){
        minkowski() {
            cube([(cell_x * grid_w) + plate_margin , (cell_y * grid_w) + plate_margin, plate_h/2]);
            cylinder(r=plate_round,h=plate_h/2);
            }
    }
}


// holes
module holes_front(){
    for (i = [ 1 : cell_y ]) {
        translate([0,grid_w*i,0])
        if (i % 2 == 0) lineH();
        else {
            if ( i == 1 || i == cell_y ) {
                //cylinder(d=6,h=hole_h);
                lineV2_bound();
            }
            else lineV2();
        }
    }
}   


module lineV2_bound(){
     for (x = [ 1 : cell_x ]) {
         translate([grid_w*x, 0, 0])
         if ( x == 1 || x == cell_x) cylinder(d=hole_d,h=hole_h);
         else {
              rotate([0,0,90]) hull() {
                translate([-hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
                translate([hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
              }
         }
        
    }
}


module lineV2(){
     for (x = [ 1 : cell_x ]) {
         translate([grid_w*x, 0, 0])
         if (x == 1 || x == cell_x ) {
                hull() {
                translate([-hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
                translate([hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
                } 
         } else {
            if (x % 2 == 1) cylinder(d=hole_d,h=hole_h);
            else {
                rotate([0,0,90]) hull() {
                translate([-hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
                translate([hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
                } 
            }
        }
    }
}

module holes_back(){
    for (i = [ 1 : cell_y ]) {
        translate([0,grid_w*i,0])
        if (i % 2 == 0) lineH();
        else lineV();
    }
}   

module lineH(){
     for (x = [ 1 : cell_x ]) {
         translate([grid_w*x, 0, 0])
         if (x % 2 == 0) cylinder(d=hole_d,h=hole_h);
         else {
              hull() {
                translate([-hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
                translate([hole_w/2,0,0])cylinder(d=hole_d,h=hole_h);
                } 
        }
    }
}

module lineV(){
     for (x = [ 1 : cell_x ]) {
         translate([grid_w*x, 0, 0])
         if (x % 2 == 1) cylinder(d=hole_d,h=hole_h);
         else {
              rotate([0,0,90])  hull() {
                translate([-hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
                translate([hole_w/2,0,0]) cylinder(d=hole_d,h=hole_h);
                } 
        }
    }
}