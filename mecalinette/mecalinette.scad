// Mecalinette
// Plans to build mecalinette > dxf for laser cutter (3mm mdf)

// Settings
hole_d=3;
hole_w=13;

grid_w=15;
plate_w=150;
plate_h=240;
cell_x = 10;
cell_y = 20;

$fn=60;

lineV();
for (y = [ 1 : 2 : cell_y ]) {
    translate([0, -grid_w*y, 0]) lineH();
    translate([0, -grid_w*(y+1), 0]) lineV();
}



// Objects
module lineH(){
    hull() {
        translate([-hole_w/2,0,0]) circle(d=hole_d);
        translate([hole_w/2,0,0]) circle(d=hole_d);
        }
    for (x = [ 1 : 2 : cell_x ]) {
        translate([grid_w*x, 0, 0]) circle(d=hole_d);
        translate([grid_w*(x+1), 0 , 0])
        hull() {
                translate([-hole_w/2,0,0]) circle(d=hole_d);
                translate([hole_w/2,0,0]) circle(d=hole_d);
            }
    }
}

module lineV(){
    for (x = [ 0 : 2 : cell_x-1 ]) {
        translate([grid_w*x, 0, 0]) circle(d=hole_d);
        translate([grid_w*(x+1), 0 , 0]) rotate([0,0,90]) 
        hull() {
                translate([-hole_w/2,0,0]) circle(d=hole_d);
                translate([hole_w/2,0,0]) circle(d=hole_d);
            }
    }
    
    translate([grid_w*cell_x, 0, 0]) circle(d=hole_d);
    
}