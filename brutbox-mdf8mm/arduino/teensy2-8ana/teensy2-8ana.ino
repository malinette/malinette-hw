/* 
 * TEENSY 2 for BRUTBOX
 * Analog sensors
 *
 * Licence : GNU/GPL3
 * Date : 2015 (revision 2021)
 * Website : http://reso-nance.org/wiki/projets/malinette-brutbox
 *
 * Midi mapping :
 * 8 analog sensors : midi controller from 1 to 8
 * 12 analog sensors allowed
 */
 
 
// Setup
const int MIDI_CHANNEL = 1;
const int SAMPLING_RATE = 40; //
const int ANA_NB = 8; // number of inputs

// Analog setup
int anaPins[] = {21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 22}; // analog pins
int anaCurrentValues[ANA_NB]; // current analog values
int anaPreviousValues[ANA_NB]; // previous analog values
 
// Sampling rate
elapsedMillis msec = 0;

void setup() { 
} 
 
void loop() {
  if (msec >= SAMPLING_RATE) {
    msec = 0;
    
    // Read analog values
    for (int i = 0; i < ANA_NB; i++) {
      anaCurrentValues[i] = (int) analogRead(anaPins[i])  / 8 ;
    }

    // Then send MIDI Control Change message
    for (int i = 0; i < ANA_NB; i++) {
      if (anaCurrentValues[i] != anaPreviousValues[i]) { // Check if current value is different from previous one
        usbMIDI.sendControlChange(i+1, anaCurrentValues[i], MIDI_CHANNEL);
        anaPreviousValues[i] = anaCurrentValues[i];
      }
    }   
  }
 
  while (usbMIDI.read()) {
    // Discard incoming MIDI messages.
  }
}
