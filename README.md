# La Malinette - Hardware
A Free Open Source Kit To Simplify Programming Interactivity

- Contact: contact_at_reso-nance.org
- Website: http://malinette.info
- Licence: GNU/GPL v3

## Description
Malinette is an all-in-one solution, a rapid prototyping tool to show and make simple interaction and multimedia projects. It is a software based on Pure Data and a hardware set based on Arduino or Teensy. See more informations on the main software part [malinette-ide](https://framagit.org/malinette/malinette-ide).


## Build & documentations
Here you can build three kind of boxes :
- standard box is like a wooden book containing an Aruino and a set of sensors and actuators
- midi box to use Arduino controllers as a Midi device
- cardboard box 

See documentations on [wiki](http://reso-nance.org/wiki/projets/malinette/hardware/accueil).

You can also build a [webcam stand](http://reso-nance.org/wiki/projets/braswebcam/accueil), by Marion Estavoyer.

All fabrication is achieved by a laser cutter machine that you can find in a Fablab.


## Brutbox
You can also build a [BrutBox](https://git.framasoft.org/resonance/brutbox/), a midi device with a sensor per box.





